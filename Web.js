
/*the reset function just displays the homepage on load at the start*/

function reset(){
    var i, content;
    content = document.getElementsByClassName("content");
    for (i = 0; i < content.length; i++) {
            content[i].style.display = "none";
        }
    document.getElementById("home").style.display = "block"
}

/*the display function takes in a tab argument and just displays it when clicked*/

function display(tab){
    var i, content;
    content = document.getElementsByClassName("content");
    for (i = 0; i < content.length; i++) {
        content[i].style.display = "none";
    }
    document.getElementById(tab).style.display = "block";
}
